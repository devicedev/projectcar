<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Backend\HomeController;
use App\Http\Controllers\FrontHomeController;
use App\Http\Controllers\Backend\OrderController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::name('web_frontend')->group(function () {
Route::get('/',[FrontHomeController::class,'index']);
Route::get('payment',[FrontHomeController::class,'payment']);
Route::get('order',[FrontHomeController::class,'order']);
Route::post('submitregister',[FrontHomeController::class,'submitregister']);
Route::post('submitlogin',[FrontHomeController::class,'login']);
Route::get('logout',[FrontHomeController::class,'logout'])->name('logout');
Route::get('type_color',[FrontHomeController::class,'type_color']);
Route::get('type_car',[FrontHomeController::class,'type_car']);

Route::post('submitbooking',[FrontHomeController::class,'submitbooking']);
Route::get('get_order',[FrontHomeController::class,'order_member']);
Route::get('get_all_order',[FrontHomeController::class,'get_all_order']);
Route::post('updateslip/{id}',[FrontHomeController::class,'updateslip']);
Route::get('deleteorder/{id}',[FrontHomeController::class,'deleteorder']);
});



Route::prefix('backend')->group(function () {
Route::get('index',[HomeController::class,'index']);
Route::get('typecar',[HomeController::class,'typecar']);
Route::get('edit_type/{id}',[HomeController::class,'edit_type']);
Route::get('show_type/{id}',[HomeController::class,'show_type']);
Route::get('add_type',[HomeController::class,'add_type']);
Route::post('add_save_type',[HomeController::class,'add_save_type']);
Route::put('editdatatype/{id}',[HomeController::class,'editdatatype']);
Route::delete('delete_type/{id}',[HomeController::class,'delete_type']);
//
Route::get('typecolor',[HomeController::class,'typecolor']);
Route::get('type_color',[HomeController::class,'type_color']);
Route::get('add_type_color',[HomeController::class,'add_type_color']);
Route::post('add_save_color',[HomeController::class,'add_save_color']);
Route::delete('delete_color/{id}',[HomeController::class,'delete_color']);
Route::get('edit_color/{id}',[HomeController::class,'edit_color']);
Route::get('show_color/{id}',[HomeController::class,'show_color']);
Route::put('editdatacolor/{id}',[HomeController::class,'editdatacolor']);
Route::get('order',[OrderController::class,'order']);
Route::get('orderdetail',[OrderController::class,'orderdetail']);
Route::get('editorder/{id}',[OrderController::class,'editorder']);
Route::get('showorder/{id}',[OrderController::class,'showorder']);
Route::put('editdataorder/{id}',[OrderController::class,'editdataorder']);

});