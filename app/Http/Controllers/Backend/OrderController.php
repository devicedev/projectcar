<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Type_car;
use App\Models\Type_color;
use App\Models\Booking_car;
use DB;

class OrderController extends Controller
{
    public function order()
    {
        return view('backend.order');
    }
    public function orderdetail()
    {
        return  Booking_car::with('type_car','type_color','members')->get();
    }
    public function editorder($id)
    {   
        $data =  Booking_car::find($id);
        return view('backend.edit_order',['data' => $data]);
    }
    public function showorder ($id)
    {   
        $data =  Booking_car::where('id',$id)->with('type_car','type_color','members')->first();
        return response()->json($data);
    }
    public function  editdataorder(Request $request,$id)
    {
        $status =   $request->input('status');
        $order = Booking_car::find($id);
       
        $order->status = $status;
 
        if($order->save())
        {
            $result = true;
        }

        return response()->json($result);
    }
 
}
