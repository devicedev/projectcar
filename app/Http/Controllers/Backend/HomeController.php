<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Type_car;
use App\Models\Type_color;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        return view('backend.index');
    }
    public function typecolor()
    {
        return view('backend.typecolor');
    }
    public function typecar()
    {
      
        return Type_car::ALL();
          
    }
    public function edit_type($id)
    {   
        $data =  Type_car::find($id);
        return view('backend.edit_type',['data' => $data]);
    }
    public function  show_type($id)
    {
        $data =  Type_car::find($id);
        return response()->json($data);
    }
    public function  editdatatype(Request $request,$id)
    {
        $name =   $request->input('name');
        $detail =   $request->input('detail');
     
        $Type_car = Type_car::find($id);
       
        $Type_car->name = $name;
        $Type_car->detail = $detail;
       
        if($Type_car->save())
        {
            $result = true;
        }

        return response()->json($result);
    }
    public function  delete_type($id)
    {
        $Type_car = Type_car::find($id);

        $Type_car->delete();
        return response()->json($Type_car);
    }
    public function add_type()
    {   
        return view('backend.add_type');
    }
    public function add_save_type(Request $request)
    {   

     $Type_car = new Type_car;

     $Type_car->name = $request->input('name');
     $Type_car->detail = $request->input('detail');

     $Type_car->save();
       
    }

    public function type_color()
    {
      
        return type_color::ALL();
          
    }
    public function add_type_color()
    {   
        return view('backend.add_color');
    }
    public function add_save_color(Request $request)
    {   

     $type_color = new type_color;

     $type_color->name = $request->input('name');
    

     $type_color->save();
       
    }
    public function  delete_color($id)
    {
        $type_color = type_color::find($id);

        $type_color->delete();
        return response()->json($type_color);
    }
    public function  edit_color($id)
    {
        $data =  type_color::find($id);
        return view('backend.edit_color',['data' => $data]);
    }
    public function  show_color($id)
    {
        $data =  type_color::find($id);
        return response()->json($data);
    }
    public function  editdatacolor(Request $request,$id)
    {
        $name =   $request->input('name');
        $type_color = type_color::find($id);
       
        $type_color->name = $name;
 
        if($type_color->save())
        {
            $result = true;
        }

        return response()->json($result);
    }
}
