<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Models\Member;
use App\Models\Type_car;
use App\Models\Type_color;
use App\Models\Booking_car;
use Illuminate\Support\Facades\Hash;
use Auth;
use Exception;
use Throwable;
use Faker\Provider\Image;
class FrontHomeController extends Controller
{
    public function index()
    {
        
        $checkauth =  Auth::guard('member')->check();
        if($checkauth==false)
        {
            $auth =0;
        }
        else{
            $auth =1;
        }
        return view('frontend.index',compact('auth'));
    }

    public function payment()
    {
        $auth ="";
        $checkauth =  Auth::guard('member')->check();
        if($checkauth==false)
        {
            $auth =0;
        }
        else{
            $auth =1;
        }
        return view('frontend.payment',compact('auth'));
    }
    
    public function order()
    {
        $auth ="";
        $checkauth =  Auth::guard('member')->check();
        if($checkauth==false)
        {
            $auth =0;
        }
        else{
            $auth =1;
        }
        return view('frontend.order',compact('auth'));
    }

    public function login(Request $request)
   
    {
      
        if(Auth::guard('member')->attempt($request->only('email','password')))
        {      
            $data =array(
                'status'=>200,
                'msg' =>'login สำเร็จ',
            );
            return response()->json($data);
      
        }
        else {
            $data =array(
                'status'=>500,
                'msg' =>'กรุณากรอกข้อมูลให้ถูกต้อง',
            );
            return response()->json($data);
        }
    
    }


    public function logout()
    {
        Auth::guard('member')->logout();

        return redirect('/');
    }
    public function submitregister(Request $request)
    {

        $post_data = $request->all();
        $post_data['password'] =  bcrypt($post_data['password']);
        if(Member::create($post_data))
        {

            $data = array(
                
                'status'=>200,
                'msg'=>'ลงทะเบียนเรียบร้อย',
            );
            return response()->json($data);
        }
        else{
            $data = array(
                'status'=>401,
                'msg'=>'ลงทะเบียนไม่สำเร็จ',
            );
            return response()->json($data);
        }

      }
    public function type_car()
    {
        return  Type_car::all();
    }
    public function type_color()
    {
        return  Type_color::all();
    }
    
    public function submitbooking(Request $request)
    {
        $post_data =  $request->all();
        $post_data['status'] = 'รอชำระเงิน';
        $post_data['member_id']   = Auth::guard('member')->id();
        try{
            Booking_car::create($post_data);
            $data =array(
                'status'=>200,
                'msg'=>'ทำรายการจองสำเร็จ',
            );

          return   response()->json($data);
        }
        catch(\Exception $e){
            return $e->getMessage();
        }
    }
    public function order_member()
    {
        return  Booking_car::with('type_car','type_color','member')->where('member_id',Auth::guard('member')->id())->where('status','!=','ตรวจสอบชำระแล้ว')->get();
    }   
    
    public function get_all_order()
    {
        return  Booking_car::with('type_car','type_color','member')->where('member_id',Auth::guard('member')->id())->get();
    } 

    public function updateslip(Request $request,$id)
    {
        try{
        $data  =    Booking_car::find($id);
        $image = $request->file('images');
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $path = public_path('/frontend/slip/');
        $image->move($path,$filename);
        $data->slip = $filename;
        $data->status ='รอการตรวจสอบ';
        $data->save();
        $response =array(
            'status'=>200,
            'msg'=>'อัพสลิปสำเร็จ กรุณารอยืนยันรายการจอง',
        );
        return response()->json($response);
        }
        catch(Exception $e){
            return  response()->json($e->getMessage());
        }

    }
    public function deleteorder($id)
    {
        Booking_car::find($id)->delete();

        return  response()->json('status',200);
    }
}
