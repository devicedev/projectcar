<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Type_car;
use App\Models\Type_color;
use App\Models\Member;
class Booking_car extends Model
{
    use HasFactory;
    protected $table = 'booking_car';


    protected $fillable = [
        'member_id',
        'color_id',
        'type_car_id',
        'book_date',
        'detail',
        'number_car',
        'status',
        'null',
    ];

    public function member(){
        return $this->hasOne(Member::class,'id');
    }
    public function type_car(){
        return $this->hasOne(Type_car::class,'id','type_car_id');
    }
    public function type_color(){
        return $this->hasOne(Type_color::class,'id','color_id');
    }
    public function members(){
        return $this->hasOne(Member::class,'id','member_id');
    }
}
