<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User  as Authentication;
class Member extends Authentication
{

    protected $table = 'member';

    protected $fillable = [
        'email',
        'password',
        'firstname',
        'lastname',
        'tel',
        'address',
    ];
}
