<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layouts.frontend.style')
    </head>
    <body class="antialiased">
        <div id="wrapper">
        @include('layouts.frontend.head')
        <div id ="app">
            @yield('content')
            
            <modal-login-component><modal-login-component>
        </div>
         </div>
        <div class="clearfix"></div>
        {{-- <footer class="footer">
        
            <div class="copy-right_text">
                <div class="container">
                    <div class="footer_border"></div>
                    <div class="row">
                        <div class="col-xl-12">
                            <p class="copy_right text-center">
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> </a>
    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer> --}}
    </body>

    @include('layouts.frontend.script')

    
</html>

