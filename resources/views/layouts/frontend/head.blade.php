<header>
    <div class="header-area ">
        <div class="header-top_area d-none d-lg-block">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-4 col-lg-4">
                        <div class="logo">
                            <a href="index.html">
                                <img src="img/logo.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-8 col-md-8">
                        <div class="header_right d-flex align-items-center">
                            <div class="short_contact_list">
                                <ul>
                                    <li><a href="#"> <i class="fa fa-envelope"></i> Projectcar@gmail.com</a></li>
                                    <li><a href="#"> <i class="fa fa-phone"></i> 02-222-2222</a></li>
                                </ul>
                            </div>
                            
                            @if(!Auth::guard('member')->check())
                            <div class="book_btn d-none d-lg-block">
                                <a href="#" data-target="#theModal-1" class ="btn btn-primary" data-toggle="modal">
                                    Login
                                </a>

                            </div>
                            @else
                            <div class="book_btn d-none d-lg-block">
                             <b style ="color:#000">  ยินดีต้อนรับ : </b>{{Auth::guard('member')->user()->firstname}}
                             <a href ="logout" class ="btn btn-danger">ออกจากระบบ</a>
                            </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="sticky-header" class="main-header-area">
            <div class="container">
                <div class="header_bottom_border">
                    <div class="row align-items-center">
                        <div class="col-12 d-block d-lg-none">
                            <div class="logo">
                                <a href="index.html">
                                    <img src="img/logo.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9">
                            <div class="main-menu  d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li ><a  href="/" >home</a></li>
                                        <li class="active"><a  href="/order">รายการจอง</a></li>
                                        <li><a href="/payment">ชำระเงิน</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</header>