<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layouts.backend.style')
    </head>
    <body class="antialiased">
        <div id="wrapper">
        @include('layouts.backend.head')
        @include('layouts.backend.navleft')
        <div id ="app">
            @yield('content')
        </div>
         </div>
        <div class="clearfix"></div>
		<footer>
			<div class="container-fluid">
				<p class="copyright">&copy; 2017 <a href="https://www.themeineed.com" target="_blank">Theme I Need</a>. All Rights Reserved.</p>
			</div>
		</footer>
    </body>

    @include('layouts.backend.script')

    
</html>

