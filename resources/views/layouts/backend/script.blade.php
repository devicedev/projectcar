<script src="{{asset('backend/assets/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('backend/assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="{{asset('backend/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('backend/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('backend/assets/vendor/chartist/js/chartist.min.js')}}"></script>
<script src="{{asset('backend/assets/scripts/klorofil-common.js')}}"></script>
<script src ="{{ mix('js/app.js') }}"></script>
